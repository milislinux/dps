module dps-yukle

go 1.21.4

require (
	github.com/aws/aws-sdk-go v1.50.30
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
)
