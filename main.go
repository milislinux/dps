package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/service/s3"
	"os"
	"gopkg.in/ini.v1"
)

func usage() {
    fmt.Printf("kullanım: %s %s %s %s\n",os.Args[0], "paket_yolu", "        paket_adı", "filebase_demet_adı")	
    fmt.Printf("örnek   : %s %s %s %s\n",os.Args[0], "abc#1.2-1...mps.lz", "abc", "      paketlerim")	
    os.Exit(1)
}
func main() {

	cfg, err := ini.Load(os.Getenv("HOME") + "/.config/filebase/config.ini")
    if err != nil {
      fmt.Printf("Aşağıdaki içeriğe sahip %s ayar dosyasını oluşturunuz!\n",os.Getenv("HOME") + "/.config/filebase/config.ini")
      fmt.Println(`
[s3]
url = https://s3.filebase.com
access-key = access-key-degeri
secret-key = secret-key-degeri
region = us-east-1
      `) 
      os.Exit(1)
    }
    URL := cfg.Section("s3").Key("url").String() // "https://s3.filebase.com"  
    access := cfg.Section("s3").Key("access-key").String()
    secret := cfg.Section("s3").Key("secret-key").String()
    region := cfg.Section("s3").Key("region").String() // "us-east-1"

	s3Config := aws.Config{
		Credentials:      credentials.NewStaticCredentials(access, secret, ""),
		Endpoint:         aws.String(URL),
		Region:           aws.String(region),
		S3ForcePathStyle: aws.Bool(true),
	}

	// create a new session using the config above and profile
	goSession, err := session.NewSessionWithOptions(session.Options{
		Config:  s3Config,
		Profile: "filebase",
	})

	// check if the session was created correctly.

	if err != nil {
		fmt.Println(err)
	}

	// create a s3 client session
	s3Client := s3.New(goSession)

    // parametre kontrol
	if len(os.Args) != 4 {
		usage()
	}
	
	filename := os.Args[1]
	key      := os.Args[2]
	bucket   := os.Args[3]

	//set the file path to upload
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println(err.Error())
	    return
	}

	defer file.Close()
	// create put object input
		putObjectInput := &s3.PutObjectInput{
		Body:   file,
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	// upload file
	_, err = s3Client.PutObject(putObjectInput)
	// print if there is an error
	if err != nil {
      fmt.Println("err:",err.Error())
      return
	}
	obj, _ := s3Client.GetObject(&s3.GetObjectInput{
      Bucket: aws.String(bucket),
      Key:    aws.String(key),
	})
	fmt.Println(*obj.Metadata["Cid"])
}

// kaynaklar
// https://docs.filebase.com/api-documentation/s3-compatible-api
